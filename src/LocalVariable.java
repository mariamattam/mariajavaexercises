

public class LocalVariable {
	
	public static void main(String args[])
	
	{
		local();
		int f = local1();
		System.out.println("local variable f ican be called outside its method by return method"+f);
		System.out.println(local1() + 100);
		TestLocalVar.test();
	}

	public static void local()
	
	{
		int a=5;
		System.out.println("local variable a is"+a);
		
	}
	
	public static int local1()
	
	{
		int b=95;//int c= 10;
		return b;
		//return c; //same as below error
		//return b,c; //throws error as only 1 value can be returned
	}
}

class TestLocalVar{
	public static void test() {
		
		int m = LocalVariable.local1();
		System.out.println(m);
		
	}
}

// a is local variable and it cannot be used outside the method local
// a is local variable because a is initialized and declared within the method local