
public class OverloadClarify {
	static int a= 10;
	static int b = 20;

	public static void main(String[] args) {
		
		sum();
		sum(90,20);
		System.out.println(sub());
		System.out.println(sum(1,2,3));

	}
	
//without return type and w/o parameters
	public static void sum() {
		int c = a + b;
		System.out.println(c);
		
	}
	
//duplicate method name though it has return type
	
	/*public static int sum() {
		int c = a + b;
		return c;   
	}  */
	
//w/o return type but with paramater
	public static void sum(int a,int b) {
		int c = a + b;
		System.out.println(c);
		
	}
	
//with return type, w/o parameter
	public static int sub() {
		int c = b- a;
		return c;
		
	}
	
//
	public static int sum(int a, int b, int d) {
		int c = b - a + d;
		return c;
		
	}

}
