

public class Final {

	final static int a = 10;
	
	public static void main(String[] args) {

		System.err.println("value of a cannot be changed  " +a);
		
	}

}

class TestFInal{
	public void testing() {
		//Final.a = 25; ///will throw an error as a is declared as final
		
	}
}
// if final keyword is used before the variable, the value of the variable cannot be changed